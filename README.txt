Black & White Flag Icon Set is an iconset for Country Icons 2.x versions.
======================================================

DESCRIPTION
------------

This module adds the possibility to display the flags icons
in grayscale providing 2 icon sets: (large, 64x64) and (small, 16x16).

REQUIREMENTS
------------
Drupal 7.x
Country Icons -> https://drupal.org/project/countryicons

INSTALLATION
------------
1.  Place the countryicons_bw module into your modules directory.
    This is normally the "sites/all/modules" directory.

2.  Go to admin/build/modules. Enable the module.
    The modules will be found in the Multilanguage section.

Read more about installing modules at http://drupal.org/node/70151


ICONS COPYRIGHT
---------------

Copyright (c) 2013 Go Squared Ltd. http://www.gosquared.com/

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.
